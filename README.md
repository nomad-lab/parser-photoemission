# parser-photoemmission

## About

This is a parser for metadata files containing photoemmission experiment metadata.

## Setup and run example

We are currently targeting Python 3.6. Some nomad dependencies might still have problems
with 3.7++. It will definitely not work with 2.x.

Best use a virtual environment:
```
virtualenv -p python3 .pyenv
source .pyenv/bin/activate
```

Clone and install the nomad infrastructure and the necessary dependencies (including this parser)
```
git clone https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR --branch photoemission nomad
git submodule update --init
pip install -r requirements.txt
./dependencies.sh -e
```

To run the parser:
```
cd nomad/dependencies/parsers/photoemission
python -m photoemissionparser <test_file>
```

## Docs

[metainfo](https://metainfo.nomad-coe.eu/nomadmetainfo_public/archive.html)

[nomad@fairdi, tmp.](http://labdev-nomad.esc.rzg.mpg.de/fairdi/nomad/latest/docs)
